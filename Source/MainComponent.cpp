/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (1300, 800);
    addAndMakeVisible(speedSlider);
    speedSlider.addListener (this);
    addAndMakeVisible(submitButton);
    submitButton.addListener (this);
    submitButton.setButtonText ("Submit");
    addAndMakeVisible(answerBox);
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    speedSlider.setBounds (50,20, 983, 40);
    submitButton.setBounds (1120,400, 100, 100);
    answerBox.setBounds ( 1120, 350, 100,50);
  
}

void MainComponent::paint (Graphics &g)
{
    //DBG ("X: " << x);
   // DBG("Y:" << y);
   // g.drawEllipse (x - getWidth()/2, y - getHeight() / 2, getWidth(), getHeight(), 1);
    g.setColour(Colours::greenyellow);
    g.fillRoundedRectangle(40,60,1000,728,10);
    
    g.setColour(Colours::black);
    
    int pCounter = 780;
    
    for (int count = 1; count < 4; count ++)
    {
        g.drawSingleLineText("A1", 15, pCounter);
        g.drawSingleLineText("A#1", 15, pCounter - 20);
        g.drawSingleLineText("B1", 15, pCounter - 40);
        g.drawSingleLineText("C1", 15, pCounter - 60);
        g.drawSingleLineText("C#1", 15, pCounter - 80);
        g.drawSingleLineText("D1", 15, pCounter - 100);
        g.drawSingleLineText("D#1", 15, pCounter - 120);
        g.drawSingleLineText("E1", 15, pCounter - 140);
        g.drawSingleLineText("F1", 15, pCounter - 160);
        g.drawSingleLineText("F#1", 15, pCounter - 180);
        g.drawSingleLineText("G1", 15, pCounter - 200);
        g.drawSingleLineText("G#1", 15, pCounter - 220);
        
        pCounter = pCounter - 240;
    }
    
    pCounter = 780;
    
    for (int count = 1; count < 4; count ++)
    {
        g.drawSingleLineText("A", 1043, pCounter);
        g.drawSingleLineText("A#1", 1043, pCounter - 20);
        g.drawSingleLineText("B1", 1043, pCounter - 40);
        g.drawSingleLineText("C1", 1043, pCounter - 60);
        g.drawSingleLineText("C#1", 1043, pCounter - 80);
        g.drawSingleLineText("D1", 1043, pCounter - 100);
        g.drawSingleLineText("D#1", 1043, pCounter - 120);
        g.drawSingleLineText("E1", 1043, pCounter - 140);
        g.drawSingleLineText("F1", 1043, pCounter - 160);
        g.drawSingleLineText("F#1", 1043, pCounter - 180);
        g.drawSingleLineText("G1", 1043, pCounter - 200);
        g.drawSingleLineText("G#1", 1043, pCounter - 220);
        
        pCounter = pCounter - 240;
    }
    
    
    
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    DBG ("Slider Value: " << speedSlider.getValue());
}

void MainComponent::buttonClicked (Button* button)
{
    DBG ("Button Clicked\n");
}
